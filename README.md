## What is this repository for? ##

This repository is an example android app that collects gear fit 2 data from Shealth. Data collected is the following:

* Total steps today
* Latest measured heart rate
* BMR Calories
* Steps Calories
* Exercise Calories
* Sleep duration over the last 24 hours
* HR over the last 0-72 hours (or 400 records)
* User profile data ( Height, Weight, Gender, age in years)


## How do I get set up? ##

see the following confluence [page](https://ssicdigitalhealth.atlassian.net/wiki/display/SDH/Gear+Fit2+Shealth+integration+example+Android+App).

## Latest APK Generated on 3/19/2017

APK Download [page](https://bitbucket.org/SSICDigitalHealth/shealthintegration/downloads/)

## Who do I talk to? ##

* Yosi Levi