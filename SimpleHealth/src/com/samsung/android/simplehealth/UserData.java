package com.samsung.android.simplehealth;

/**
 * class used to hold user profile data.
 */

public class  UserData{
    float AgeInYears;
    float WeightinKg;
    float HeightinCm;
    int   Gender;

    UserData()
    {
        AgeInYears = 0;
        WeightinKg = 0;
        HeightinCm = 0;
        Gender = 0;
    }
}

