/**
 * Copyright (C) 2014 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Mobile Communication Division,
 * Digital Media & Communications Business, Samsung Electronics Co., Ltd.
 *
 * This software and its documentation are confidential and proprietary
 * information of Samsung Electronics Co., Ltd.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to
 * any electronic medium or machine-readable form without the prior written
 * consent of Samsung Electronics.
 *
 * Samsung Electronics makes no representations with respect to the contents,
 * and assumes no responsibility for any errors that might appear in the
 * software and documents. This publication and the contents hereof are subject
 * to change without notice.
 */

package com.samsung.android.simplehealth;

import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthData;
import com.samsung.android.sdk.healthdata.HealthDataObserver;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataResolver.Filter;
import com.samsung.android.sdk.healthdata.HealthDataResolver.ReadRequest;
import com.samsung.android.sdk.healthdata.HealthDataResolver.ReadResult;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthDevice;
import com.samsung.android.sdk.healthdata.HealthDeviceManager;
import com.samsung.android.sdk.healthdata.HealthResultHolder;

import android.database.Cursor;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

/*******************************************************************************
 * @brief Main class to collect data from Shealth
 * @return none
 */
public class SHealthReporter {
    private final HealthDataStore mStore;
    public SHealthReporter(HealthDataStore store) {
        mStore = store;
    }   // Constractor
    private UserData mUd = new UserData();                              // user profile
    private String mFit2UUID;                                           // connected fit UUID
    private int mStepTotalCalories;                                     // total calories from step count
    private int mExerciseTotalCalories;                                 // total calories from exercise
    private float mExerciseActiveDuration;                              // total excercise duration [ms]
    private float mStepActiveDuration;                                  // total steps duration [ms]
    private int mExerciseWalkCalories;                                  // Calories originated from exercise type 1001 (walking)
    private float mExerciseWalkeDuration;                               // Duration of exercise type 1001 (walking) [ms]

    /*******************************************************************************
     * @brief Class start function to init all class variables
     * @return none
     */
    public void start(UserData ud) {

        // Set UI Controls
        MainActivity.getInstance().HrHistorySeekBar.setProgress(24);
        MainActivity.getInstance().HrHistorySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int progress = MainActivity.getInstance().HrHistorySeekBar.getProgress();
                MainActivity.getInstance().Get24HRButton.setText("Last "+progress+" Hours HR");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = MainActivity.getInstance().HrHistorySeekBar.getProgress();
                MainActivity.getInstance().Get24HRButton.setText("Last "+progress+" Hours HR");
            }
        });


        MainActivity.getInstance().Get24HRButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                read24Hr(MainActivity.getInstance().HrHistorySeekBar.getProgress());
            }
        });


        // Register an observer to listen changes of step count and get today step count
        HealthDataObserver.addObserver(mStore, HealthConstants.StepCount.HEALTH_DATA_TYPE, mObserver);
        HealthDataObserver.addObserver(mStore, HealthConstants.HeartRate.HEALTH_DATA_TYPE, mObserver);
        HealthDataObserver.addObserver(mStore, HealthConstants.Sleep.HEALTH_DATA_TYPE, mObserver);
        HealthDataObserver.addObserver(mStore, HealthConstants.Exercise.HEALTH_DATA_TYPE, mObserver);
        HealthDataObserver.addObserver(mStore, HealthConstants.SleepStage.HEALTH_DATA_TYPE, mObserver);

        mUd = ud;                   // todo: need to check that profile is set properly !!!!
        mFit2UUID = getFit2Uuid();  // todo: need to check that fit2 is connected !!!!
        mExerciseTotalCalories = 0;
        mExerciseActiveDuration = 0;
        mStepTotalCalories = 0;
        mStepActiveDuration = 0;
        mExerciseWalkCalories = 0;
        mExerciseWalkeDuration = 0;

        // Do not change order of the function calls
        readTodayStepCountDuaration();      // Step Count Duration
        readTodayStepCalories();            // Step Count Calories
        readTodayExerciseData();            // exercise calories
        readLastHr();                       // Last HR
        readTodaySleep();                   // 24 Hour window sleep


    }

    /*******************************************************************************
     * @brief read cumulative exercise duration and calories from midnight today till now
     * @return none
     */
    private void readTodayExerciseData() {
        HealthDataResolver resolver = new HealthDataResolver(mStore, null);
        HealthData data = new HealthData();

        long startTime = getStartTimeOfToday();  // midnight
        long endTime = System.currentTimeMillis(); // now

        //fit2 only
        Filter filter = Filter.and(Filter.greaterThanEquals(HealthConstants.Exercise.START_TIME, startTime),
                Filter.lessThanEquals(HealthConstants.Exercise.START_TIME, endTime),
                Filter.eq(HealthConstants.Exercise.DEVICE_UUID,mFit2UUID));

        HealthDataResolver.ReadRequest request_exercise = new ReadRequest.Builder()
                .setDataType(HealthConstants.Exercise.HEALTH_DATA_TYPE)
                .setProperties(new String[] {HealthConstants.Exercise.CALORIE,HealthConstants.Exercise.DURATION,HealthConstants.Exercise.EXERCISE_TYPE})
                .setFilter(filter)
                .build();

        try {
            resolver.read(request_exercise).setResultListener(mExerciseListener);

        } catch (Exception e) {
            Log.e(MainActivity.APP_TAG, e.getClass().getName() + " - " + e.getMessage());
            Log.e(MainActivity.APP_TAG, "Getting Excercise data fails.");
        }
    }

    private final HealthResultHolder.ResultListener<ReadResult> mExerciseListener = new HealthResultHolder.ResultListener<ReadResult>() {
        @Override
        public void onResult(ReadResult result) {
            int type=0;
            int calories=0;
            int activity_time=0;
            int walk_calories=0;
            int walk_activity_time=0;
            Cursor c = null;

            try {
                c = result.getResultCursor();
                if (c != null) {
                    while (c.moveToNext()) {
                        type = c.getInt(c.getColumnIndex(HealthConstants.Exercise.EXERCISE_TYPE));

                        activity_time += c.getInt(c.getColumnIndex(HealthConstants.Exercise.DURATION));
                        calories+= c.getInt(c.getColumnIndex(HealthConstants.Exercise.CALORIE));

                        // we need to account for walking activity calories in the same time of
                        // step count calories to avoid calorie duplication
                        if(type == 1001)
                        {
                            walk_calories += c.getInt(c.getColumnIndex(HealthConstants.Exercise.CALORIE));
                            walk_activity_time += c.getInt(c.getColumnIndex(HealthConstants.Exercise.DURATION));
                        }

                        Log.d(MainActivity.APP_TAG, "Exec Duration:"+activity_time+ " Calories:" + calories);

                    }
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }

            mExerciseTotalCalories = calories;
            mExerciseActiveDuration = activity_time;
            mExerciseWalkCalories = walk_calories;
            mExerciseWalkeDuration = walk_activity_time;

            Log.d(MainActivity.APP_TAG, "mExerciseListener, Ex Cal: "+mExerciseTotalCalories+ " Step Cal: " + mStepTotalCalories);
            updateCalorieDisplay();
        }
    };


    /*******************************************************************************
     * @brief read cumulative step count calories only from midnight today till now.
     * we do not get duration from this api since it is not accurate
     * @return none
     */
    public void readTodayStepCalories() {

        // Suppose that the required permission has been acquired already
        HealthDeviceManager healthDeviceManager = new HealthDeviceManager(mStore);

        long startTime = getStartTimeOfToday(); //midnight
        long endTime = System.currentTimeMillis(); // now

        // Create a filter for fit2 only
        Filter filter = Filter.eq("source_type",1);

        ReadRequest request = new ReadRequest.Builder()
                // Set the data type
                .setDataType("com.samsung.shealth.step_daily_trend")
                // Set the source type with the filter
                .setFilter(filter)
                // Set the sort order
                .setSort("day_time", HealthDataResolver.SortOrder.DESC)
                // Build
                .build();

        HealthDataResolver resolver = new HealthDataResolver(mStore, null);

        try {
            resolver.read(request).setResultListener(mStepCalorieListener);
        } catch (Exception e) {
            Log.e(MainActivity.APP_TAG, e.getClass().getName() + " - " + e.getMessage());
        }
    }

    private final HealthResultHolder.ResultListener<ReadResult> mStepCalorieListener =
            new HealthResultHolder.ResultListener<ReadResult>() {

                @Override
                public void onResult(ReadResult result) {

                    Cursor c = null;
                    int stepCount = 0;
                    int calories = 0;

                    try {
                        c = result.getResultCursor();
                        if (c != null) {
                            while(c.moveToNext()) {
                                stepCount= c.getInt(c.getColumnIndex("count"));
                                calories += c.getInt(c.getColumnIndex("calorie"));

                                Log.d(MainActivity.APP_TAG, "Step Count step_daily_trend: " + stepCount + " Cal:"+ calories );
                                break;  // if there is more than one -> get the latest one only
                            }
                        } else {
                            Log.d(MainActivity.APP_TAG, "The curor is null.");
                        }
                    } catch(Exception e) {
                        Log.d(MainActivity.APP_TAG, e.getClass().getName() + " - " + e.getMessage());
                    } finally {
                        if (c != null) {
                            c.close();
                        }

                    }

                    mStepTotalCalories = calories;
                    updateCalorieDisplay();
                }
            };

    /*******************************************************************************
     * @brief read cumulative step count duration from midnight today till now.
     * we do not get calories from this api since it is not accurate
     * @return none
     */
    public void readTodayStepCountDuaration() {

        // Suppose that the required permission has been acquired already
        HealthDeviceManager healthDeviceManager = new HealthDeviceManager(mStore);

        long startTime = getStartTimeOfToday();  //midnight
        long endTime = System.currentTimeMillis(); //now

        // Create a filter for fit2 only
        Filter filter = Filter.and(Filter.eq(HealthConstants.StepCount.DEVICE_UUID,mFit2UUID),
                Filter.lessThanEquals(HealthConstants.StepCount.START_TIME, endTime),Filter.greaterThanEquals(HealthConstants.StepCount.START_TIME, startTime));

        ReadRequest request = new ReadRequest.Builder()
                // Set the data type
                .setDataType(HealthConstants.StepCount.HEALTH_DATA_TYPE)
                .setProperties(new String[] {HealthConstants.StepCount.START_TIME,HealthConstants.StepCount.END_TIME,
                                             HealthConstants.StepCount.COUNT,HealthConstants.StepCount.CALORIE,
                                             HealthConstants.StepCount.SPEED,HealthConstants.StepCount.DISTANCE})
                // Set the source type with the filter
                .setFilter(filter)
                // Build
                .build();

        HealthDataResolver resolver = new HealthDataResolver(mStore, null);

        try {
            resolver.read(request).setResultListener(mStepCountDurationListener);
        } catch (Exception e) {
            Log.e(MainActivity.APP_TAG, e.getClass().getName() + " - " + e.getMessage());
        }
    }

    private final HealthResultHolder.ResultListener<ReadResult> mStepCountDurationListener =
            new HealthResultHolder.ResultListener<ReadResult>() {

                @Override
                public void onResult(ReadResult result) {

                    Cursor c = null;
                    long StartTime = 0;
                    long EndTime = 0;
                    int count=0;
                    int StepCount = 0;
                    int calories=0;
                    int calories_total=0;
                    long Activity_time=0;
                    float speed=0;
                    float distance=0;
                    float effective_activity_time=0;
                    try {
                        c = result.getResultCursor();
                        if (c != null) {
                            while(c.moveToNext()) {
                                StartTime= c.getLong(c.getColumnIndex(HealthConstants.StepCount.START_TIME));
                                EndTime= c.getLong(c.getColumnIndex(HealthConstants.StepCount.END_TIME));
                                count= c.getInt(c.getColumnIndex(HealthConstants.StepCount.COUNT));
                                calories= c.getInt(c.getColumnIndex(HealthConstants.StepCount.CALORIE));
                                distance= c.getFloat(c.getColumnIndex(HealthConstants.StepCount.DISTANCE));
                                speed= c.getFloat(c.getColumnIndex(HealthConstants.StepCount.SPEED));

                                StepCount+=count;
                                calories_total += calories;
                                Activity_time += EndTime-StartTime;  // this proved to be inaccurate
                                effective_activity_time+= distance/speed;  // formula --> s=v*t
                                Log.d(MainActivity.APP_TAG, "Step Duration:"+ (EndTime-StartTime)+" Effective Duration: "+ effective_activity_time  + " Count:" + count +" total:" + StepCount + " cal:" + calories + " total:" + calories_total);

                            }
                        } else {
                            Log.d(MainActivity.APP_TAG, "The cursor is null.");
                        }
                    } catch(Exception e) {
                        Log.d(MainActivity.APP_TAG, e.getClass().getName() + " - " + e.getMessage());
                    } finally {
                        if (c != null) {
                            c.close();
                        }
                    }

                    mStepActiveDuration=(effective_activity_time*1000);//Activity_time;
                    MainActivity.getInstance().drawStepCount(String.valueOf(StepCount));
                    updateCalorieDisplay();
                    Log.d(MainActivity.APP_TAG, "mStepCountDurationeListener: "+mStepActiveDuration+  "  Step Count:" + StepCount + "  Calories: " + calories_total);

                }
            };

    /*******************************************************************************
     * @brief read cumulative sleep duration over a 24 hour time window
     * @return none
     */
    private void readTodaySleep() {
        HealthDataResolver resolver = new HealthDataResolver(mStore, null);

        // Set time range from start time of today to the current time with offset of 12 hours back
        // to cover night time of yesterday
        long startTime = System.currentTimeMillis()-(24*60*60*1000); //current time less 24 hours
        long endTime = System.currentTimeMillis(); //current time


        Filter filter = Filter.and(Filter.greaterThanEquals(HealthConstants.Sleep.START_TIME, startTime),
                Filter.lessThanEquals(HealthConstants.Sleep.END_TIME, endTime),
                Filter.eq(HealthConstants.Sleep.DEVICE_UUID,mFit2UUID));

        HealthDataResolver.ReadRequest start_request = new ReadRequest.Builder()
                .setDataType(HealthConstants.Sleep.HEALTH_DATA_TYPE)
                .setProperties(new String[] {HealthConstants.Sleep.START_TIME,HealthConstants.Sleep.END_TIME})
                .setFilter(filter)
                .build();

        try {
            resolver.read(start_request).setResultListener(mSleepListener);

        } catch (Exception e) {
            Log.e(MainActivity.APP_TAG, e.getClass().getName() + " - " + e.getMessage());
            Log.e(MainActivity.APP_TAG, "Getting Sleep fails.");
        }
    }

    private final HealthResultHolder.ResultListener<ReadResult> mSleepListener = new HealthResultHolder.ResultListener<ReadResult>() {
        @Override
        public void onResult(ReadResult result) {
            String SleepTime ="";
            long SleepTimeEnd=0;
            long SleepTimeStart=0;
            long sleep_time_total = 0;
            Cursor c = null;


            try {
                c = result.getResultCursor();
                if (c != null) {
                    while (c.moveToNext()) {
                        SleepTimeStart = c.getLong(c.getColumnIndex(HealthConstants.Sleep.START_TIME));
                        SleepTimeEnd = c.getLong(c.getColumnIndex(HealthConstants.Sleep.END_TIME));
                        sleep_time_total += (SleepTimeEnd - SleepTimeStart);
                        Log.d(MainActivity.APP_TAG, "Sleep Duration:"+((float)sleep_time_total)/(1000*60*60));

                    }
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }

            long hours = sleep_time_total/(1000*60*60);
            float minutes = ((((float)sleep_time_total)/(1000*60*60)) % (float)hours)*60;

            SleepTime= hours+" h "+ ((int)minutes +" m");
            MainActivity.getInstance().drawSleep(String.valueOf(SleepTime));
        }
    };



    /*******************************************************************************
     * @brief read last recorded Heart Rate originated from fit2 device
     * @return none
     */
    private void readLastHr() {
        HealthDataResolver hr_resolver = new HealthDataResolver(mStore, null);

        Filter filter = Filter.eq(HealthConstants.HeartRate.DEVICE_UUID,mFit2UUID);

        HealthDataResolver.ReadRequest request = new ReadRequest.Builder()
                .setDataType(HealthConstants.HeartRate.HEALTH_DATA_TYPE)
                .setProperties(new String[] {HealthConstants.HeartRate.HEART_RATE})
                .setFilter(filter)
                .setSort(HealthConstants.HeartRate.CREATE_TIME,HealthDataResolver.SortOrder.DESC)
                .build();

        try {
            hr_resolver.read(request).setResultListener(mHrListener);
        } catch (Exception e) {
            Log.e(MainActivity.APP_TAG, e.getClass().getName() + " - " + e.getMessage());
            Log.e(MainActivity.APP_TAG, "Getting HR fails.");
        }
    }


    private final HealthResultHolder.ResultListener<ReadResult> mHrListener = new HealthResultHolder.ResultListener<ReadResult>() {
        @Override
        public void onResult(ReadResult result) {
            int beats = 0;
            Cursor c = null;

            try {
                c = result.getResultCursor();
                if (c != null) {
                    while (c.moveToNext()) {
                        beats = c.getInt(c.getColumnIndex(HealthConstants.HeartRate.HEART_RATE));
                        Log.d(MainActivity.APP_TAG, "HR:"+beats);

                        break; // get the latest HR only
                    }
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }
            MainActivity.getInstance().drawHr(String.valueOf(beats));
        }
    };


    /*******************************************************************************
     * @brief read HR over a 24 hour time window
     * @return none
     */
    private void read24Hr(int Hours) {
        HealthDataResolver resolver = new HealthDataResolver(mStore, null);

        // Set time range from start time of today to the current time with offset of 12 hours back
        // to cover night time of yesterday
        long startTime = System.currentTimeMillis()-(Hours*60*60*1000); //current time less 24 hours
        long endTime = System.currentTimeMillis(); //current time


        Filter filter = Filter.and(Filter.greaterThanEquals(HealthConstants.HeartRate.CREATE_TIME, startTime),
                Filter.lessThanEquals(HealthConstants.HeartRate.CREATE_TIME, endTime));

        HealthDataResolver.ReadRequest start_request = new ReadRequest.Builder()
                .setDataType(HealthConstants.HeartRate.HEALTH_DATA_TYPE)
                .setProperties(new String[] {HealthConstants.HeartRate.HEART_RATE,HealthConstants.HeartRate.END_TIME})
                .setFilter(filter)
                .build();

        try {
            resolver.read(start_request).setResultListener(m24HrListener);

        } catch (Exception e) {
            Log.e(MainActivity.APP_TAG, e.getClass().getName() + " - " + e.getMessage());
            Log.e(MainActivity.APP_TAG, "Getting Sleep fails.");
        }
    }

    private final HealthResultHolder.ResultListener<ReadResult> m24HrListener = new HealthResultHolder.ResultListener<ReadResult>() {
        @Override
        public void onResult(ReadResult result) {
            long[] HrTimeEnd= new long[500];
            long[] Hr = new long[500];
            Cursor c = null;
            int i = 0;

            try {
                c = result.getResultCursor();
                if (c != null) {
                    while (c.moveToNext()) {
                        Hr[i] = c.getLong(c.getColumnIndex(HealthConstants.HeartRate.HEART_RATE));
                        HrTimeEnd[i] = c.getLong(c.getColumnIndex(HealthConstants.HeartRate.END_TIME));
                        Log.d(MainActivity.APP_TAG, "++++++++++++++HR:"+Hr[i] +" Timestamp:"+HrTimeEnd[i]);
                        i++;
                        if(i>500) {
                            Log.d(MainActivity.APP_TAG, "++++++++++++++ Error - m24HrListener - Array ran out of space");
                            break;
                        }
                    }
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }

            if(i>0)
            {
                try {
                    writeToFile(Hr,HrTimeEnd,i);
                    Toast.makeText(MainActivity.getInstance(), "File Saved! ("+ i +" Records)", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    Log.d(MainActivity.APP_TAG, "++++++++++++++ Error - m24HrListener - File Write failed");
                    Toast.makeText(MainActivity.getInstance(), "File Saving Failed!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            else
            {
                Toast.makeText(MainActivity.getInstance(), "No Records Found!", Toast.LENGTH_LONG).show();
            }
            //long hours = sleep_time_total/(1000*60*60);
            //float minutes = ((((float)sleep_time_total)/(1000*60*60)) % (float)hours)*60;

            //SleepTime= hours+" h "+ ((int)minutes +" m");
            //MainActivity.getInstance().drawSleep(String.valueOf(SleepTime));
        }
    };






    /*******************************************************************************
    * @brief find the connected fit2 device
    * @return fit2 UUID string or "NoDeviceFound" string
    */
    private String getFit2Uuid(){
        String uuid="NoDeviceFound";
        HealthDeviceManager healthDeviceManager = new HealthDeviceManager(mStore);
        List<HealthDevice> devices = healthDeviceManager.getAllDevices();
        ArrayList<String> sourceDevices = new ArrayList<String>();

        for(HealthDevice healthDevice : devices) {
            Log.e(TAG, "Health Device Name = " + healthDevice.getCustomName());
            if (healthDevice.getCustomName().equalsIgnoreCase("Gear Fit2")) {
                uuid = healthDevice.getUuid();
                break;
            }

        }
        return uuid;
    }

    /*******************************************************************************
     * @brief Set to today midnight and return in millisecond
     * @return millisecond value reprsenting last midnight time (epoch time value)
     */
    private long getStartTimeOfToday() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        return today.getTimeInMillis();
    }

    /*******************************************************************************
     * @brief get BMR calories based on user profile ant current time of the day.
     * @in:
     *      exercise_duration - exercise duration in ms
     *      step_corrected_duration - step count duration corrected to remove activity time if the
     *      activity was walking (1001) so we do not duplicate calorie count.
     * @return BMR value
     *
     *
        Energy Balance - formula originated from MBU fit2 engineering team
          1. Daily calorie balance
            = Intake Calorie – TDEE (Total daily energy expenditure)
            = Intake Calorie – TDEE ( Activity burned calories + resting BMR )

          2. Resting BMR
            = BMR * (1440 mins – B mins)/1440
            - 1440 mins: 24h * 60m
            - B mins: active minutes
            - C kcal: Burned calories for active minutes

          3. BMR ( Basal Metabolic Rate )
            - for men, BMR = 88.362 + ( 13.397 x weight in kg ) + ( 4.799 x height in cm ) - ( 5.677 x age in years )
            - for women, BMR = 447.593 + ( 9.247 x weight in kg ) + ( 3.098 x height in cm ) - (4.330 x age in years )

     */
    private float getBMRcalories(float exercise_duration,float step_corrected_duration) {
        float bmr = 0;
        float total_active_min = (float)(((exercise_duration+step_corrected_duration) / (1000*60)));  // convert from millisecond to minutes
        float bmr_calories = 0;

        // claculate how many minutes out of the 24 hours (1440) have passed so far
        long midnight = getStartTimeOfToday();
        long current_time = System.currentTimeMillis();
        float minutes_so_far = (float) ((current_time-midnight)/(1000*60)); // maximum value is 1440

        if (mUd.Gender == 1 ) //male
        {
            //24 hour bmr
            bmr = (float) (88.362 + (13.397 * mUd.WeightinKg) + (4.799 * mUd.HeightinCm) - (5.677 * mUd.AgeInYears));

            //relative bmr --> function of how much of the day passed so far and how many active mintues
            bmr_calories =  (bmr * ((minutes_so_far - total_active_min) / 1440));
        }
        else if (mUd.Gender == 2) //female
        {
            //24 hour bmr
            bmr = (float) (447.593 + (9.247 * mUd.WeightinKg) + (3.098 * mUd.HeightinCm) - (4.330 * mUd.AgeInYears));

            //relative bmr --> function of how much of the day passed so far and how many active mintues
            bmr_calories = (bmr * ((minutes_so_far - total_active_min) / 1440));
        } else {
            // no gender - no calcualtion
            //todo: handle error situation
        }

        return bmr_calories;
    }

    /*******************************************************************************
     * @brief call to update BMR and calories as BMR is accumulated  per passing time.
     * @return none
     *
     */
    private void updateCalorieDisplay()
    {
        int bmr_calories = 0;

        // if the user did walking activity (1001) and logged it as exercise
        // we need to deduct this from the step count calories and duration
        int step_corrected_calories = mStepTotalCalories - mExerciseWalkCalories;
        float step_corrected_duration =  mStepActiveDuration - mExerciseWalkeDuration;

        bmr_calories = (int) getBMRcalories(mExerciseWalkeDuration,step_corrected_duration);
        MainActivity.getInstance().drawCalorieCount(step_corrected_calories,bmr_calories ,mExerciseTotalCalories);
    }


    public void writeToFile(long[] Hr, long[] Ts, int HrRecordsNum ) throws IOException {
        //Log.d("in download start", downloadStart.toString());
        int progress = MainActivity.getInstance().HrHistorySeekBar.getProgress();
        String dirName = "HR_Collection_TimeStamp";
        String fileName = "HR_"+progress+"_"+System.currentTimeMillis()+".csv";
        File outputDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), dirName);
        int i;
        if (!outputDirectory.exists()) {
            outputDirectory.mkdir();
        }
        File file = new File(outputDirectory, fileName);
        if(!file.exists())
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("TAG", "exception=" + "11");

            }
        FileWriter fOut = new FileWriter(file, true);
        BufferedWriter bwr = new BufferedWriter(fOut);

        //Log.d("inside writetofile", "write=" + stream);
        try{
            //Write to csv file
            if (bwr != null ) {
                for(i=0;i<HrRecordsNum;i++) {
                    bwr.write(Ts[i] + "," + Hr[i] + "\n");
                }
                bwr.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", "exception=" + "10");

        }
        //attachmentFile = outputDirectory + "/" + fileNames[caseStream];
        //dataFile = new File(attachmentFile);
    }


    /*******************************************************************************
     * @brief Shealth observer objects - updates values whenever Shealth syncs with fit2.
     */
    private final HealthDataObserver mObserver = new HealthDataObserver(null) {

        // Update the step count when a change event is received
        @Override
        public void onChange(String dataTypeName) {

            Log.d(MainActivity.APP_TAG, "Observer receives a data changed event " + dataTypeName);


            if(dataTypeName.equals("com.samsung.health.step_count") ) {
                readTodayStepCountDuaration();      // Step Count Duration
                readTodayStepCalories();            // Step Count Calories
            }else if(dataTypeName.equals("com.samsung.health.exercise")) {
                readTodayExerciseData();            // exercise calories
            } else if (dataTypeName.equals("com.samsung.health.heart_rate")) {
                readLastHr();                       // Last HR
            } else if (dataTypeName.equals("com.samsung.health.sleep")) {
                readTodaySleep();                   // 24 Hour window sleep
            }  else {
                Log.d(MainActivity.APP_TAG, "Observer data changed event " + dataTypeName + " not applicable ");
                //todo: handle error
            }


        }
    };


}
