/**
 * Copyright (C) 2014 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Mobile Communication Division,
 * Digital Media & Communications Business, Samsung Electronics Co., Ltd.
 *
 * This software and its documentation are confidential and proprietary
 * information of Samsung Electronics Co., Ltd.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to
 * any electronic medium or machine-readable form without the prior written
 * consent of Samsung Electronics.
 *
 * Samsung Electronics makes no representations with respect to the contents,
 * and assumes no responsibility for any errors that might appear in the
 * software and documents. This publication and the contents hereof are subject
 * to change without notice.
 */

package com.samsung.android.simplehealth;

import com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthDataService;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthPermissionManager;
import com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionKey;
import com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionResult;
import com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionType;
import com.samsung.android.sdk.healthdata.HealthResultHolder;
import com.samsung.android.sdk.healthdata.HealthUserProfile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MainActivity extends Activity {

    public static final String APP_TAG = "SimpleHealth";
    private final int MENU_ITEM_PERMISSION_SETTING = 1;

    private static MainActivity mInstance = null;
    private HealthDataStore mStore;
    private HealthConnectionErrorResult mConnError;
    private Set<PermissionKey> mKeySet;
    private SHealthReporter mReporter;
    public Button Get24HRButton;                 // Get Hours Button
    public SeekBar HrHistorySeekBar;                 // Get  Hours History

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        Get24HRButton = (Button)this.findViewById(R.id.button24Hr);
        HrHistorySeekBar = (SeekBar) this.findViewById(R.id.seekBarCollectTime);

        // Set all required permissions
        mInstance = this;
        mKeySet = new HashSet<PermissionKey>();
        mKeySet.add(new PermissionKey(HealthConstants.StepCount.HEALTH_DATA_TYPE, PermissionType.READ));
        mKeySet.add(new PermissionKey(HealthConstants.Sleep.HEALTH_DATA_TYPE, PermissionType.READ));
        mKeySet.add(new PermissionKey(HealthConstants.HeartRate.HEALTH_DATA_TYPE, PermissionType.READ));
        mKeySet.add(new PermissionKey(HealthConstants.Exercise.HEALTH_DATA_TYPE, PermissionType.READ));
        mKeySet.add(new PermissionKey("com.samsung.shealth.step_daily_trend", PermissionType.READ));
        mKeySet.add(new PermissionKey(HealthConstants.USER_PROFILE_DATA_TYPE, PermissionType.READ));
        mKeySet.add(new PermissionKey(HealthConstants.SleepStage.HEALTH_DATA_TYPE, PermissionType.READ));

        HealthDataService healthDataService = new HealthDataService();
        try {
            healthDataService.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create a HealthDataStore instance and set its listener
        mStore = new HealthDataStore(this, mConnectionListener);
        // Request the connection to the health data store
        mStore.connectService();
    }

    @Override
    public void onDestroy() {
        mStore.disconnectService();
        super.onDestroy();
    }

    private final HealthDataStore.ConnectionListener mConnectionListener = new HealthDataStore.ConnectionListener() {

        @Override
        public void onConnected() {
            Log.d(APP_TAG, "Health data service is connected.");
            HealthPermissionManager pmsManager = new HealthPermissionManager(mStore);
            mReporter = new SHealthReporter(mStore);  // this class does the Shealth data collection

            try {
                // Check whether the permissions that this application needs are acquired
                Map<PermissionKey, Boolean> resultMap = pmsManager.isPermissionAcquired(mKeySet);

                if (resultMap.containsValue(Boolean.FALSE)) {
                    // Request the permission for reading step counts if it is not acquired
                    pmsManager.requestPermissions(mKeySet, MainActivity.this).setResultListener(mPermissionListener);
                } else {
                    // Get the current step count and display it
                    mReporter.start(GetUserProfile());
                }
            } catch (Exception e) {
                Log.e(APP_TAG, e.getClass().getName() + " - " + e.getMessage());
                Log.e(APP_TAG, "Permission setting fails.");
            }
        }

        @Override
        public void onConnectionFailed(HealthConnectionErrorResult error) {
            Log.d(APP_TAG, "Health data service is not available.");
            showConnectionFailureDialog(error);
        }

        @Override
        public void onDisconnected() {
            Log.d(APP_TAG, "Health data service is disconnected.");
        }
    };

    private final HealthResultHolder.ResultListener<PermissionResult> mPermissionListener =
            new HealthResultHolder.ResultListener<PermissionResult>() {

                @Override
                public void onResult(PermissionResult result) {
                    Log.d(APP_TAG, "Permission callback is received.");
                    Map<PermissionKey, Boolean> resultMap = result.getResultMap();

                    if (resultMap.containsValue(Boolean.FALSE)) {
                        drawStepCount("");
                        drawCalorieCount(0,0,0);
                        drawHr("");
                        drawSleep("");
                        showPermissionAlarmDialog();
                    } else {
                        // Get the current step count and display it
                        // Gets the user profile if permission is acquired
                        mReporter.start(GetUserProfile());
                    }
                }
            };

    /*******************************************************************************
     * @brief Connects to SHealth to pull the user profile details
     * @return populated UserData class instance
     */
    private UserData GetUserProfile() {
        HealthUserProfile usrProfile = HealthUserProfile.getProfile(mStore);
        UserData ud = new UserData();

        if(usrProfile==null)
        {
                Toast.makeText(this, "Error: No User Profile Set In Shealth!", Toast.LENGTH_LONG).show();
                //set garbage values
                ud.AgeInYears=0;
                ud.Gender = HealthUserProfile.GENDER_UNKNOWN;
                ud.HeightinCm = 0.0f;
                ud.WeightinKg = 0.0f;
        }
        else {
            // Date of birth - yyyymmdd
            String birthDate = usrProfile.getBirthDate();
            if (birthDate.isEmpty()) {
                Log.d(APP_TAG, "Date of birth is not set by user yet.");
            } else {
                //calculate age in years - required for BMR
                int year = Integer.parseInt(birthDate.substring(0, 4));
                ud.AgeInYears = (float) (Calendar.getInstance().get(Calendar.YEAR) - year) - 1; // (-1) to round down;
            }
            // Gender
            ud.Gender = usrProfile.getGender();
            if (ud.Gender == HealthUserProfile.GENDER_UNKNOWN) {
                Log.d(APP_TAG, "Gender is not set by user yet.");
            }

            // Height
            ud.HeightinCm = usrProfile.getHeight();
            if (ud.HeightinCm == 0.0f) {
                Log.d(APP_TAG, "Height is not set by user yet.");
            }

            // Weight
            ud.WeightinKg = usrProfile.getWeight();
            if (ud.WeightinKg == 0.0f) {
                Log.d(APP_TAG, "Weight is not set by user yet.");
            }

            //Samsung account ID
            String accountID = usrProfile.getUserId();
            if (accountID.isEmpty()) {
                Log.d(APP_TAG, "Samsung account ID is not set by user yet.");
            }
        }
        // Display User profile on UI
        TextView UserProfileTv = (TextView) findViewById(R.id.editHealthDateValue6);
        String Gender[]={"U","M","F"};

        UserProfileTv.setText(((int) ud.WeightinKg)+" Kg, " + ((int) ud.HeightinCm)+" cm, " +
                ((int) ud.AgeInYears)+" Years, " + Gender[ud.Gender]);
        return (ud);
    }

    /*******************************************************************************
     * @brief Display step count data on UI
     * @return none
     */
    public void drawStepCount(String count) {

        TextView stepCountTv = (TextView) findViewById(R.id.editHealthDateValue1);
        // Display the today step count so far
        stepCountTv.setText(count);
    }

    /*******************************************************************************
     * @brief Display calorie data on UI
     * @return none
     */
    public void drawCalorieCount(int StepCalories,int BMRCalories, int ExerciseCalories) {

        TextView CaloriesCountTv = (TextView) findViewById(R.id.editHealthDateValue3);

        CaloriesCountTv.setText(String.valueOf(StepCalories+BMRCalories+ExerciseCalories));

        CaloriesCountTv = (TextView) findViewById(R.id.editHealthDateValue5);
        CaloriesCountTv.setText(String.valueOf(StepCalories) +" steps, "+ String.valueOf(ExerciseCalories)+" exer.");
    }

    /*******************************************************************************
     * @brief Display Last Heart rate data on UI
     * @return none
     */
    public void drawHr(String Beats) {

        TextView HrTv = (TextView) findViewById(R.id.editHealthDateValue2);

        // Display the today HR so far
        HrTv.setText(Beats);
    }

    /*******************************************************************************
     * @brief Display Sleep Duration on UI
     * @return none
     */
    public void drawSleep(String time) {

        TextView SleepTv = (TextView) findViewById(R.id.editHealthDateValue4);

        SleepTv.setText(time);
    }


    /*******************************************************************************
     * @brief Display Sleep Duration on UI
     * @return none
     */
    public void get24Hr() {

        //TextView SleepTv = (Button) findViewById(R.id.editHealthDateValue4);


    }

    /*******************************************************************************
     * @brief Error handling and service functions
     * @return none
     */
    public static MainActivity getInstance() {
        return mInstance;
    }

    private void showPermissionAlarmDialog() {
        if (isFinishing()) {
            return;
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("Notice");
        alert.setMessage("All permissions should be acquired");
        alert.setPositiveButton("OK", null);
        alert.show();
    }

    private void showConnectionFailureDialog(HealthConnectionErrorResult error) {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        mConnError = error;
        String message = "Connection with S Health is not available";

        if (mConnError.hasResolution()) {
            switch (error.getErrorCode()) {
                case HealthConnectionErrorResult.PLATFORM_NOT_INSTALLED:
                    message = "Please install S Health";
                    break;
                case HealthConnectionErrorResult.OLD_VERSION_PLATFORM:
                    message = "Please upgrade S Health";
                    break;
                case HealthConnectionErrorResult.PLATFORM_DISABLED:
                    message = "Please enable S Health";
                    break;
                case HealthConnectionErrorResult.USER_AGREEMENT_NEEDED:
                    message = "Please agree with S Health policy";
                    break;
                default:
                    message = "Please make S Health available";
                    break;
            }
        }

        alert.setMessage(message);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if (mConnError.hasResolution()) {
                    mConnError.resolve(mInstance);
                }
            }
        });

        if (error.hasResolution()) {
            alert.setNegativeButton("Cancel", null);
        }

        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.add(1, MENU_ITEM_PERMISSION_SETTING, 0, "Connect to S Health");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {

        if (item.getItemId() == (MENU_ITEM_PERMISSION_SETTING)) {
            HealthPermissionManager pmsManager = new HealthPermissionManager(mStore);
            try {
                // Show user permission UI for allowing user to change options
                pmsManager.requestPermissions(mKeySet, MainActivity.this).setResultListener(mPermissionListener);
            } catch (Exception e) {
                Log.e(APP_TAG, e.getClass().getName() + " - " + e.getMessage());
                Log.e(APP_TAG, "Permission setting fails.");
            }
        }

        return true;
    }
}



